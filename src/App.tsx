import React from 'react';
import Layout from './components/Layout';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Welcome from './screens/Welcome';

import ListUsers from './screens/users/ListUsers';
import AddUser from './screens/users/AddUser';
import EditUser from './screens/users/EditUser';

import Placeholder from './screens/Placeholder';

/**
 * Main Application component.
 * Primary role is to handle routing
 */
export default function App() {
	return (
		<Router>
			<Layout>
				<Switch>
					<Route exact path="/" component={Welcome} />
					<Route exact path="/users" component={ListUsers} />
					<Route exact path="/users/new" component={AddUser} />
					<Route exact path="/users/:id" component={EditUser} />
					<Route exact path="/orders"><Placeholder title="Orders" /></Route>
					<Route exact path="/inventory"><Placeholder title="Inventory" /></Route>
					<Route exact path="/deliveries"><Placeholder title="Deliveries" /></Route>
					<Route exact path="/settings"><Placeholder title="Settings" /></Route>
				</Switch>
			</Layout>
		</Router>
	);
}
