import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import type { RootState, AppDispatch } from './store';

/**
 * Statically typed version of react-redux's `useDispatch`
 */
export const useAppDispatch = () => useDispatch<AppDispatch>();

/**
 * Statically typed version of react-redux's `useSelector`
 */
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
