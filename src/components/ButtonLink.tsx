import React, { forwardRef, useMemo } from 'react';
import { Link } from 'react-router-dom';
import Button, { ButtonProps } from '@material-ui/core/Button';

export interface ButtonLinkProps extends ButtonProps {
	to: string;
}

/**
 * A Material UI styled Button that works as a React Router Link
 */
export default function ButtonLink(props: ButtonLinkProps) {
	const { to } = props;

	const CustomLink = useMemo(
		() => forwardRef<HTMLAnchorElement>(function CustomLink(linkProps, ref) {
			return <Link ref={ref} to={to} {...linkProps} />;
		}),
		[to],
	);

	const buttonProps = {
		component: CustomLink,
		...props,
	};
	return (
		<Button {...buttonProps} />
	);
}
