import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	toolbar: {
		gridArea: 'foot',
		height: theme.footerHeight,
		padding: theme.spacing(2),
		textAlign: 'right',
	},
}));

export interface MainFooterProps {
	children: React.ReactNode,
}

/**
 * Footer that appears at the bottom of the application
 */
export default function MainFooter(props: MainFooterProps) {
	const styles = useStyles();

	return (
		<div className={styles.toolbar}>
			{props.children}
		</div>
	);
}
