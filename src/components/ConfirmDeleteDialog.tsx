import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

interface ConfirmDeleteDialogProps {
	label: string;
	body?: string | React.ReactNode;
	onConfirm?: () => void;
	onCancel?: () => void;
	onClose?: () => void;
	onExited?: () => void;
}

/**
 * Dialog box asking the user to confirm deleting a record
 */
export default function ConfirmDeleteDialog(props: ConfirmDeleteDialogProps) {
	const { label } = props;
	const [open, setOpen] = useState(true);
	
	let body = props.body;
	if (!body) {
		body = <DialogContentText>Are you sure you want to delete &apos;{label}&apos;?</DialogContentText>;
	}
	else if (typeof body === 'string') {
		body = <DialogContentText>{body}</DialogContentText>;
	}

	function handleConfirm() {
		setOpen(false);
		props.onConfirm?.();
		props.onClose?.();
	}

	function handleCancel() {
		setOpen(false);
		props.onCancel?.();
		props.onClose?.();
	}

	return (
		<Dialog onClose={handleCancel} onExited={props.onExited} open={open}>
			<DialogTitle>Delete {label}?</DialogTitle>
			<DialogContent>
				{body}
			</DialogContent>
			<DialogActions>
				<Button onClick={handleCancel} color="primary">
					Cancel
				</Button>
				<Button onClick={handleConfirm} color="primary" autoFocus>
					Delete {label}
				</Button>
			</DialogActions>
		</Dialog>
	);
}
