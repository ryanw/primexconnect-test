import React from 'react';
import List from '@material-ui/core/List';
import ListItemLink from './ListItemLink';

import WelcomeIcon from '@material-ui/icons/MeetingRoom';
import UsersIcon from '@material-ui/icons/People';
import OrdersIcon from '@material-ui/icons/Receipt';
import SettingsIcon from '@material-ui/icons/Settings';
import DeliveriesIcon from '@material-ui/icons/LocalShipping';
import InventoryIcon from '@material-ui/icons/MenuBook';

/**
 * Main sidebar navigation with links to all sections of the application
 */
export default function MainFooter() {
	return (
		<List component="nav">
			<ListItemLink to="/" icon={<WelcomeIcon />}>
				Welcome
			</ListItemLink>

			<ListItemLink to="/users" icon={<UsersIcon />}>
				Users
			</ListItemLink>

			<ListItemLink to="/orders" icon={<OrdersIcon />}>
				Orders
			</ListItemLink>

			<ListItemLink to="/inventory" icon={<InventoryIcon />}>
				Inventory
			</ListItemLink>

			<ListItemLink to="/deliveries" icon={<DeliveriesIcon />}>
				Deliveries
			</ListItemLink>

			<ListItemLink to="/settings" icon={<SettingsIcon />}>
				Settings
			</ListItemLink>
		</List>
	);
}
