import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Container, { ContainerProps } from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
	content: {
		flex: 1,
		padding: 0,
		overflow: 'auto',
		'& > p:first-child': {
			marginTop: 0,
		}
	},
	padded: {
		padding: theme.spacing(2),
	},
}));

export interface ContentProps {
	maxWidth?: ContainerProps['maxWidth'];
	children: React.ReactNode;
	padded?: boolean;
}

/**
 * Main page content wrapper
 */
export default function Content(props: ContentProps) {
	const styles = useStyles();

	return (
		<Container maxWidth={props.maxWidth} className={clsx(styles.content, props.padded && styles.padded)}>
			{props.children}
		</Container>
	);
}

