import React, { forwardRef, useMemo } from 'react';
import MaterialLink from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';


export interface LinkProps {
	to: string;
	children?: React.ReactNode;
}

/**
 * A Material UI styled Link that works as a React Router Link
 */
export default function Link(props: LinkProps) {
	const { to, children } = props;

	const CustomLink = useMemo(
		() => forwardRef<HTMLAnchorElement>(function CustomLink(linkProps, ref) {
			return <RouterLink ref={ref} to={to} {...linkProps} />;
		}),
		[to],
	);

	return (
		<MaterialLink component={CustomLink}>
			{children}
		</MaterialLink>
	);
}
