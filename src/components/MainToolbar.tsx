import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';

const useStyles = makeStyles((theme) => ({
	toolbar: {
		height: 64,
		padding: theme.spacing(2),
		zIndex: 100,
	},
	title: {
		flex: 1,
	},
}));

export interface MainToolbarProps {
	title?: string,
	children?: React.ReactNode,
}

/**
 * Toolbar that contains the page title and action buttons
 */
export default function MainToolbar(props: MainToolbarProps) {
	const styles = useStyles();

	return (
		<Toolbar className={styles.toolbar}>
			{props.title &&
				<Typography component="h1" variant="h6" color="inherit" noWrap className={styles.title}>
					{props.title}
				</Typography>
			}
			{props.children}
		</Toolbar>
	);
}
