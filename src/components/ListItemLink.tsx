import React, { forwardRef, useMemo } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText, { ListItemTextProps } from '@material-ui/core/ListItemText';


export interface ListItemLinkProps extends ListItemProps {
	to: string;
	icon?: React.ReactNode;
	primary?: ListItemTextProps['primary'];
	children?: React.ReactNode;
}

/**
 * A Material UI styled ListItem that works as a React Router NavLink
 */
export default function ListItemLink(props: ListItemLinkProps) {
	const { icon, primary, to, children } = props;
	const location = useLocation();

	const CustomLink = useMemo(
		() => forwardRef<HTMLAnchorElement>(function CustomLink(linkProps, ref) {
			return <NavLink ref={ref} to={to} {...linkProps} />;
		}),
		[to],
	);

	return (
		<li>
			<ListItem selected={location.pathname === to} button component={CustomLink}>
				{icon && <ListItemIcon>{icon}</ListItemIcon>}
				{children && <ListItemText primary={primary}>{children}</ListItemText>}
			</ListItem>
		</li>
	);
}
