import React, { forwardRef, useMemo } from 'react';
import { Link } from 'react-router-dom';
import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';

export interface IconButtonLinkProps extends IconButtonProps {
	to: string;
}

/**
 * A Material UI styled IconButton that works as a React Router Link
 */
export default function IconButtonLink(props: IconButtonLinkProps) {
	const { to } = props;

	const CustomLink = useMemo(
		() => forwardRef<HTMLAnchorElement>(function CustomLink(linkProps, ref) {
			return <Link ref={ref} to={to} {...linkProps} />;
		}),
		[to],
	);

	const buttonProps = {
		component: CustomLink,
		...props,
	};
	return (
		<IconButton {...buttonProps} />
	);
}
