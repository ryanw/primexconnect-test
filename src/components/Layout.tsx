import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import MaterialLink from '@material-ui/core/Link';
import Snackbar from '@material-ui/core/Snackbar';
import { useAppSelector, useAppDispatch } from '../hooks';
import { hideToast } from '../actions';
import MainFooter from './MainFooter';
import MainNavigation from './MainNavigation';
import theme from '../theme';

import LogoImage from '../../assets/logo.png';

const useStyles = makeStyles((theme) => ({
	root: {
		flex: 1,
		display: 'grid',
		gridTemplateAreas: `
			"bar  bar"
			"side main"
			"side foot"
		`,
		gridTemplateRows: `${theme.headerHeight}px auto ${theme.footerHeight}px`,
		gridTemplateColumns: `${theme.drawerWidth}px auto`,
	},

	pageBar: {
		gridArea: 'bar',
		height: theme.headerHeight,
		padding: theme.spacing(0, 2),
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		textShadow: '0 2px 3px rgba(0, 0, 0, 0.5)',
		'& a': {
			color: 'inherit',
			textDecoration: 'none',
			display: 'flex',
			alignItems: 'center',

			'&:hover': {
				textDecoration: 'underline',
			}
		},
	},

	logo: {
		height: 48,
		width: 48,
		marginRight: theme.spacing(2),
	},

	drawer: {
		gridArea: 'side',
		overflow: 'auto',
	},

	drawerPaper: {
		position: 'static',
		width: `${theme.drawerWidth}px`,
		padding: theme.spacing(2),
	},
	main: {
		gridArea: 'main',
		display: 'flex',
		flexDirection: 'column',
		overflow: 'auto',
	},
}));

export interface LayoutProps {
	children: React.ReactNode,
}

/**
 * The main application layout, including header, sidebar and main content area
 */
export default function Layout(props: LayoutProps) {
	return (
		<ThemeProvider theme={theme}>
			<LayoutContent {...props} />
		</ThemeProvider>
	);
}

function LayoutContent(props: LayoutProps) {
	const styles = useStyles();
	const [snackOpen, setSnackOpen] = useState(false);
	const toast = useAppSelector(state => state.toast);
	const dispatch = useAppDispatch();

	useEffect(() => {
		setSnackOpen(!!toast);
	}, [toast]);

	return (
		<div className={styles.root}>
			<CssBaseline />
			<AppBar className={styles.pageBar}>
				<Link to="/">
					<img src={LogoImage} className={styles.logo} />
					<Typography component="h1" variant="h4" color="inherit" noWrap>
						Meat Space
					</Typography>
				</Link>
			</AppBar>
			<Drawer variant="permanent" className={styles.drawer} classes={{ paper: styles.drawerPaper }}>
				<MainNavigation />
			</Drawer>
			<main className={styles.main}>
				{props.children}
			</main>
			<MainFooter>Copyright 2021 © <MaterialLink href="mailto:hire@ryan.nz">Ryan Williams</MaterialLink></MainFooter>
			<Snackbar open={snackOpen} message={toast} autoHideDuration={5000} onClose={() => setSnackOpen(false)} onExited={() => dispatch(hideToast())} />
		</div>
	);
}
