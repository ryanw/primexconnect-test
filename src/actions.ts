/**
 * Create a Redux action to show a simple popup notifiction
 */
export function showToast(message: string) {
	return { type: 'SHOW_TOAST', message };
}

/**
 * Create a Redux action to hide any open popup notifications
 */
export function hideToast() {
	return { type: 'HIDE_TOAST' };
}
