import { createMuiTheme } from '@material-ui/core/styles';

declare module '@material-ui/core/styles/createMuiTheme' {
	interface Theme {
		drawerWidth: React.CSSProperties['width'],
		headerHeight: React.CSSProperties['height'],
		footerHeight: React.CSSProperties['height'],
	}

	interface ThemeOptions {
		drawerWidth?: React.CSSProperties['width'],
		headerHeight?: React.CSSProperties['height'],
		footerHeight?: React.CSSProperties['height'],
	}
}

export default createMuiTheme({
	drawerWidth: 250,
	headerHeight: 64,
	footerHeight: 48,
	palette: {
		primary: {
			main: '#6C4DE7',
		},
		secondary: {
			main: '#E76C4D',
		},
	},
});
