import React from 'react';
import Link from '../components/Link';
import Content from '../components/Content';
import MainToolbar from '../components/MainToolbar';

/**
 * The initial welcome screen
 */
export default function Welcome() {
	return (
		<>
			<MainToolbar title="Welcome" />
			<Content padded>
				<p>
					Welcome to my implementation of the PrimeXConnect Frontend Developer&apos;s Test.
				</p>
				<p>
					Use the navigation on the left to visit the <Link to="/users">Users</Link> sections where you can create, view, and edit users.
					Most other links simply point to a placeholder page.
				</p>
			</Content>
		</>
	);
}
