import React from 'react';
import Content from '../components/Content';
import MainToolbar from '../components/MainToolbar';

export interface PlaceholderProps {
	title: string;
}

/**
 * A simple placeholder for pages that don't yet exist
 */
export default function Placeholder(props: PlaceholderProps) {
	return (
		<>
			<MainToolbar title={props.title} />
			<Content padded>
				{props.title} will go here
			</Content>
		</>
	);
}
