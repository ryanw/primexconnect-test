import React, { useState } from 'react';
import { useFirebase, useFirebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Link from '@material-ui/core/Link';
import { User } from '../../schema';
import { useAppSelector, useAppDispatch } from '../../hooks';
import { showToast } from '../../actions';
import MainToolbar from '../../components/MainToolbar';
import Content from '../../components/Content';
import ButtonLink from '../../components/ButtonLink';
import IconButtonLink from '../../components/IconButtonLink';
import ConfirmDeleteDialog from '../../components/ConfirmDeleteDialog';

/**
 * Maximum records to fetch pre-page
 */
const PAGE_SIZE = 10;

/**
 * Number of pixels to scroll from the bottom before triggering the next page load
 */
const SCROLL_EDGE = 20;

const useStyles = makeStyles((theme) => ({
	table: {
		maxHeight: '100%',
		padding: theme.spacing(0, 2),
	},
	actions: {
		whiteSpace: 'nowrap',
		'& > *': {
			visibility: 'hidden',
		}
	},
	row: {
		'&:hover $actions > *': {
			visibility: 'visible',
		}
	},
	tableActions: {
		padding: theme.spacing(2),
		textAlign: 'center',
	}
}));


/**
 * Table view screen of all Users currently in the system
 */
export default function ListUser() {
	const styles = useStyles();
	const [limit, setLimit] = useState(PAGE_SIZE);

	// Fetch 1 extra row so we can easily test if there's another page
	const queryParams = ['orderByChild=name', `limitToFirst=${limit + 1}`];
	useFirebaseConnect({ path: 'users', queryParams });

	const users = useAppSelector(state => {
		if (!state.firebase.ordered?.users) {
			return state.firebase.ordered?.users;
		}
		if (Array.isArray(state.firebase.ordered?.users)) {
			return state.firebase.ordered.users;
		}
	});


	function loadMore() {
		setLimit(limit => limit + PAGE_SIZE);
	}

	// Automatically load more when scrolling to the bottom
	function handleScroll(e: React.UIEvent<HTMLDivElement>) {
		// Ignore if we don't have all the records or we've reached the end of the dataset
		if (users.length < limit) {
			return;
		}

		const el = e.target as HTMLDivElement;
		const scrollTop = el.scrollTop;
		const scrollLimit = el.scrollHeight - el.clientHeight;

		if (scrollLimit - scrollTop < SCROLL_EDGE) {
			loadMore();
		}
	}

	return (
		<>
			<MainToolbar title="Users">
				<ButtonLink to="/users/new" variant="contained" color="primary" startIcon={<AddIcon />}>
					Create User
				</ButtonLink>
			</MainToolbar>

			{!isLoaded(users) && <Content padded>Loading users...</Content>}
			{isLoaded(users) && isEmpty(users) && <Content padded>No users found</Content>}

			{isLoaded(users) && !isEmpty(users) &&
				<TableContainer className={styles.table} onScroll={handleScroll}>
					<UserTable users={users?.slice(0, limit)} />
					{users.length > limit &&
						<div className={styles.tableActions}>
							<Button variant="contained" color="primary" onClick={loadMore}>Fetch More Users</Button>
						</div>
					}
				</TableContainer>
			}
		</>
	);
}


interface UserTableProps {
	users: { key: string, value: User }[];
}

/**
 * Table view of all users currently in the system
 */
function UserTable(props: UserTableProps) {
	const { users } = props;

	const styles = useStyles();
	const firebase = useFirebase();
	const dispatch = useAppDispatch();
	const [deleteUserId, setDeleteUserId] = useState<string>(null);
	const deleteUser = deleteUserId && getUser(deleteUserId);



	function getUser(id: string): User | null {
		const user = users.find(u => u.key === id);
		return user?.value;
	}

	function removeUser(id: string) {
		const user = users.find(u => u.key === id);
		const name = user?.value?.name;
		firebase.remove(`users/${id}`);
		dispatch(showToast(`Deleted User '${name}'`));
	}

	function handleDelete() {
		removeUser(deleteUserId);
	}

	return (
		<>
			<Table stickyHeader>
				<TableHead>
					<TableRow>
						<TableCell>Name</TableCell>
						<TableCell>Email</TableCell>
						<TableCell>Roles</TableCell>
						<TableCell>Organisation</TableCell>
						<TableCell>Organisation Features</TableCell>
						<TableCell>Country</TableCell>
						<TableCell></TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{users?.map(({ key: id, value: user }) => (
						<TableRow hover key={id} className={styles.row}>
							<TableCell>{user.name}</TableCell>
							<TableCell>{user.email && <Link href={`mailto:${user.email}`} title={`Email ${user.email}`}>{user.email}</Link>}</TableCell>
							<TableCell>{user.role}</TableCell>
							<TableCell>{user.organization}</TableCell>
							<TableCell>{user.features?.join(', ')}</TableCell>
							<TableCell>{user.country}</TableCell>
							<TableCell align="right" className={styles.actions}>
								<IconButtonLink to={`/users/${id}`} title="Edit"><EditIcon /></IconButtonLink>
								<IconButton onClick={() => setDeleteUserId(id)} title="Delete"><DeleteIcon /></IconButton>
							</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
			{deleteUser && <ConfirmDeleteDialog label={deleteUser.name} onConfirm={handleDelete} onExited={() => setDeleteUserId(null)} />}
		</>
	);
}


