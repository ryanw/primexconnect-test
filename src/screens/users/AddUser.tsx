import React from 'react';
import { useHistory } from 'react-router-dom';
import { v4 as uuid } from 'uuid';
import UserForm from './UserForm';

/**
 * Screen where you can add a new user to the system
 */
export default function EditUser() {
	const id = uuid();
	const history = useHistory();

	return (
		<UserForm id={id} onSave={() => history.push('/users')} />
	);
}
