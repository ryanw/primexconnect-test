import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useFirebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import { useAppSelector } from '../../hooks';
import MainToolbar from '../../components/MainToolbar';
import Content from '../../components/Content';
import UserForm from './UserForm';

/**
 * Screen where you can edit an existing user
 */
export default function EditUser() {
	const { id } = useParams<{id: string}>();
	useFirebaseConnect({ path: `/users/${id}` });
	const user = useAppSelector(state => state.firebase.data.users?.[id]);
	const history = useHistory();

	if (!isLoaded(user)) {
		return (
			<>
				<MainToolbar title={'Edit User'} />
				<Content padded>
					<div>Loading user details...</div>
				</Content>
			</>
		);
	}

	if (isEmpty(user)) {
		return (
			<>
				<MainToolbar title={'404 Not Found'} />
				<Content padded>
					<div>User doesn&apos;t exist</div>
				</Content>
			</>
		);
	}

	return (
		<UserForm id={id} user={user} onSave={() => history.push('/users')} />
	);
}
