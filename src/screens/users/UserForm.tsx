import React, { useState, useRef } from 'react';
import { useFirebase } from 'react-redux-firebase';
import { makeStyles } from '@material-ui/core/styles';
import { useAppDispatch } from '../../hooks';
import { showToast } from '../../actions';
import { User, Roles, Features } from '../../schema';
import Content from '../../components/Content';
import CountrySelect from '../../components/CountrySelect';
import MainToolbar from '../../components/MainToolbar';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
	form: {
		padding: theme.spacing(2),
		'& form input[type="submit"]': {
			display: 'none',
		},
		'& form': {
			display: 'flex',
			flexDirection: 'column',
			'& > *': {
				marginBottom: theme.spacing(2),
			}
		}
	},
}));

export interface UserFormProps {
	id: string;
	user?: Partial<User>;
	onSave?: (user: Partial<User>) => void;
}

type ValidationErrors = { [key in keyof Partial<User>]: string };

/**
 * Form user on the Add and Edit user screens
 */
export default function UserForm(props: UserFormProps) {
	const { id, user = {} } = props;
	const styles = useStyles();
	const dispatch = useAppDispatch();

	const submitButton = useRef(null);
	const firebase = useFirebase();
	const [changes, setChanges] = useState<Partial<User>>({});
	const [errors, setErrors] = useState<ValidationErrors>({});

	function getValue<T extends keyof User>(name: T): Partial<User>[T] {
		if (changes[name] != null) {
			return changes[name] as User[T];
		} else {
			return user[name] || '' as User[T];
		}
	}

	function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
		setChanges(changes => ({
			...changes,
			[event.target.name]: event.target.value,
		}));
	}

	function handleChangeCountry(_event: React.ChangeEvent, value: string) {
		setChanges(changes => ({
			...changes,
			country: value,
		}));
	}

	function handleSubmit(e: React.FormEvent) {
		e.preventDefault();
		saveChanges();
	}

	function saveChanges() {
		const isValid = validateChanges();
		if (isValid) {
			firebase.update(`users/${id}`, changes);
			props.onSave?.({ ...user, ...changes });
			dispatch(showToast(`Saved User '${changes.name || user.name}'`));
		}
		else {
			dispatch(showToast('Error saving User'));
		}
	}

	function validateChanges(): boolean {
		const errors: ValidationErrors = {};

		let isValid = true;
		for (const field of ['name', 'email', 'role', 'organization', 'country'] as (keyof User)[]) {
			const value = getValue(field);
			if (!value || (typeof value === 'string' && !value.replace(/\s/g, '')) || (Array.isArray(value) && value.length === 0)) {
				isValid = false;
				errors[field] = 'must not be blank';
			}
		}

		setErrors(errors);
		return isValid;
	}

	return (
		<>
			<MainToolbar title={`Edit User '${getValue('name') || 'Unnamed User'}'`}>
				<Button onClick={() => submitButton.current?.click()} variant="contained" color="primary" startIcon={<SaveIcon />}>
					Save Changes
				</Button>
			</MainToolbar>
			<Content padded>
				<Container maxWidth="sm" component={Paper} className={styles.form}>
					<form onSubmit={handleSubmit}>
						<FormControl>
							<TextField required error={!!errors['name']} helperText={errors['name']} onChange={handleChange} name="name" label="Name" value={getValue('name')} autoFocus />
						</FormControl>
						<FormControl>
							<TextField required error={!!errors['email']} type="email" helperText={errors['email']} onChange={handleChange} name="email" label="Email" value={getValue('email')} />
						</FormControl>
						<FormControl>
							<InputLabel required>Role</InputLabel>
							<Select required error={!!errors['role']} value={getValue('role')} onChange={handleChange} name="role" input={<Input />}>
								{Roles.map((role) => (
									<MenuItem key={role} value={role}>
										{role}
									</MenuItem>
								))}
							</Select>
							{errors['role'] && <FormHelperText error>{errors['role']}</FormHelperText>}
						</FormControl>
						<FormControl>
							<TextField required error={!!errors['organization']} helperText={errors['organization']} onChange={handleChange} name="organization" label="Organisation" value={getValue('organization')} />
						</FormControl>
						<FormControl>
							<InputLabel>Features</InputLabel>
							<Select multiple error={!!errors['features']} value={getValue('features') || []} onChange={handleChange} name="features" input={<Input />}>
								{Features.map((feature) => (
									<MenuItem key={feature} value={feature}>
										{feature}
									</MenuItem>
								))}
							</Select>
							{errors['features'] && <FormHelperText error>{errors['features']}</FormHelperText>}
						</FormControl>
						<FormControl>
							<CountrySelect required error={!!errors['country']} helperText={errors['country']} label="Country" value={getValue('country') || null} onChange={handleChangeCountry} name="country" />
						</FormControl>

						<input type="submit" ref={submitButton} />
					</form>
				</Container>
			</Content>
		</>
	);
}
