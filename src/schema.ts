export const Roles = ['Owner', 'Employee'] as const;
export type Role = typeof Roles[number];

export const Features = ['Trade Vault', 'Inventory', 'Analytics'] as const;
export type Feature = typeof Features[number];

/**
 * A User inside the Firebase database
 */
export type User = {
	id: string;
	name: string;
	email: string;
	organization: string;
	role: Role;
	features: Feature[];
	country: string;
}

/**
 * Our Firebase database schema
 */
export interface Schema {
	users: User;
}

export type Profile = User;
