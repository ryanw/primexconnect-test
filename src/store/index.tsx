import React from 'react';
import { Provider } from 'react-redux';
import firebaseConfig from '../config/firebase';
import firebase from 'firebase/app';
import 'firebase/database';
import { createStore, combineReducers } from 'redux';
import { ReactReduxFirebaseProvider, firebaseReducer, FirebaseReducer } from 'react-redux-firebase';
import { Schema, Profile } from '../schema';
import toastReducer, { INITIAL_STATE as TOAST_INITIAL_STATE } from './toastReducer';

const INITIAL_STATE = {
	toast: TOAST_INITIAL_STATE,
};

firebase.initializeApp(firebaseConfig);

export const store = createStore(combineReducers({
	toast: toastReducer,
	firebase: firebaseReducer,
}), INITIAL_STATE);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState> & {
	firebase: FirebaseReducer.Reducer<Profile, Schema>;
};

const providerConfig = {
	userProfile: 'users',
};

export interface StoreProviderProps {
	children: React.ReactNode,
}

/**
 * Provider that includes both the Redux store and the Firebase providers
 */
export function StoreProvider(props: StoreProviderProps) {
	return (
		<Provider store={store}>
			<ReactReduxFirebaseProvider firebase={firebase} config={providerConfig} dispatch={store.dispatch}>
				{props.children}
			</ReactReduxFirebaseProvider>
		</Provider>
	);
}
