import { AnyAction } from 'redux';

export const INITIAL_STATE = '';

/**
 * Reducer to Toast actions
 */
export default function toastReducer(message = INITIAL_STATE, action: AnyAction): string {
	switch (action.type) {
	case 'SHOW_TOAST':
		return action.message;

	case 'HIDE_TOAST':
		return '';

	default:
		return message;
	}
}
