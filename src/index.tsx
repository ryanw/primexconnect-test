import React from 'react';
import { StoreProvider } from './store';
import { render } from 'react-dom';
import App from './App';

render(
	<StoreProvider>
		<App />
	</StoreProvider>,
	document.querySelector('#app')
);


// Hide loading message
const loadingMessage = document.querySelector('.loading');
loadingMessage?.parentNode.removeChild(loadingMessage);
