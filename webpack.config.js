const path = require('path');
const webpack = require('webpack');


const production = (process.env.NODE_ENV === 'production');

module.exports = {
	mode: production ? 'production' : 'development',
	entry: {
		main: './src/index.tsx',
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	devtool: 'source-map',
	devServer: {
		host: '0.0.0.0',
		port: 8080,
		historyApiFallback: true,
	},
	module: {
		rules: [
			// Typescript
			{ test: /\.tsx?$/, use: 'ts-loader' },

			// Images
			{ test: /\.(png|svg|jpg|jpeg|gif)$/i, type: 'asset/resource' },
		],
	},
	resolve: {
		extensions: [ '.tsx', '.ts', '.js' ],
	},
	plugins: [
		new webpack.DefinePlugin({
			'PRODUCTION': JSON.stringify(production),
		}),
	],
};
