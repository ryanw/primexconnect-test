# Ryan's PrimeXConnect Test Implementation

This is my version of the PrimeXConnect Frontend Developer's test.

## Getting Started

First install all the packages. We're assuming you have Node + NPM ready to go.

    npm install

Once that has completed, you can start the webpack dev server:

    npm run server

And visit <http://localhost:8080/> to see it in action.

If you wish to use your own Firebase database then edit the config in ./src/config/firebase.ts

## Deployment

To build a production artifact simply run:

    NODE_ENV=production npm run build

This will create artifacts in _./dist/_. Deploy these along with _index.html_ and the _./assets_ directory to any webserver.


## Demo

A live demo is available at <https://primex.ryan.nz/>

## Contact

You can email me at <hire@ryan.nz>
